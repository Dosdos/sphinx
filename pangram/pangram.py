#!/bin/python

# Complete the function below.
AZ = "abcdefghijklmnopqrstuvwxyz"


def is_pangram(n):
    """ Write you solution here """
    n = n.lower()

    if len(n) < len(AZ):
        return 0

    tmp = [0 for i in range(len(AZ))]

    for c in n:
        if c in AZ:  # O(n)
            tmp[AZ.index(c)] = 1

    return 0 if (0 in tmp) else 1


# n = raw_input()
n = "Return 1, if N is a pangram, otherwise return 0."
n = "The quick brown fox jumps over the lazy dog"
# n = "We promptly judged antique ivory buckles for the next prize    "
n = "We promptly judged antique ivory buckles for the prize    "

res = is_pangram(n)

print res
